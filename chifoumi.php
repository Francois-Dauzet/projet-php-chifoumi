<?php

/**
 * 1 = pierre -> 3 - ciseaux
 * 2 = feuille -> 1 - pierre
 * 3 = ciseaux -> 2 - feuille
 */

$choixJ1 = 0;
$choixJ2 = 0;

$scoreJ1 = 0;
$scoreJ2 = 0;

// c'est la boucle de la partie
while ($scoreJ1 < 3 && $scoreJ2 < 3) {

    print('Choix du joueur 1 ? (1 = pierre  / 2 = feuille / 3 = ciseaux)');
    $choixJ1 = intval(trim(fgets(STDIN)));
    print('Choix du joueur 2 ? (1 = pierre  / 2 = feuille / 3 = ciseaux)');
    $choixJ2 = intval(trim(fgets(STDIN)));

    if (($choixJ1 == 1 && $choixJ2 == 3) ||
        ($choixJ1 == 2 && $choixJ2 == 1) ||
        ($choixJ1 == 3 && $choixJ2 == 2)
    ) {
        $scoreJ1++;
        print('Bravo le joueur 1 !');
    } else if (($choixJ2 == 1 && $choixJ1 == 3) ||
        ($choixJ2 == 2 && $choixJ1 == 1) ||
        ($choixJ2 == 3 && $choixJ1 == 2)
    ) {
        $scoreJ2++;
        print('Bravo le joueur 2 !');
    } else {
        print('Match nul !');
    }
}

if ($scoreJ1 == 3) {
    print('Bravo joueur 1 tu as gagné la partie !');
} else {
    print('Bravo joueur 2 tu as gagné la partie !');
}